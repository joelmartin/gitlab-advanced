## Objective ##

Run a gitlab runner for your personal projects, using NERSC login nodes.

**Nota Bene** This will run as you, with access to _all_ of your files. Think very carefully before doing this! Check with a consultant if you're in any doubt about it.

## Exercise ##

Pick a NERSC host, e.g. Genepool, and log in. Check that the $NERSC_HOST environment variable is set correctly.

Download one of the gitlab runner binaries. See https://docs.gitlab.com/runner/install/linux-manually.html for details. The **amd64** flavor is your best bet. The example commands they give assume root access, you can modify them as follows:

```
mkdir -p ~/bin
cd ~/bin ~/.gitlab-runner
wget -O gitlab-runner https://gitlab-ci-multi-runner-downloads.s3.amazonaws.com/latest/binaries/gitlab-ci-multi-runner-linux-amd64
chmod +x gitlab-runner
```

Go to your project's **Settings** -> **CI/CD Pipelines** tab and note the URL and **registration token** to use. The registration token is specific to your project, **do not** give it out to other people, it's a security risk if you do that. It can be used to register a malicious runner that installs a trojan in your code, which could then compromise the account of anyone who runs your code later on.

**Register** your runner with this command:

```
./gitlab-runner register --config $HOME/.gitlab-runner/config.$NERSC_HOST.toml
```

Answer the questions as they come up. Here's a sample session for me, run on May 23rd 2017. Things change, so it might be different if you're doing this at another time.

```
> ./gitlab-runner register --config $HOME/.gitlab-runner/config.$NERSC_HOST.toml
WARNING: Running in user-mode.                     
WARNING: The user-mode requires you to manually start builds processing: 
WARNING: $ gitlab-runner run                       
WARNING: Use sudo for system-mode:                 
WARNING: $ sudo gitlab-runner...                   
                                                   
Please enter the gitlab-ci coordinator URL (e.g. https://gitlab.com/):
https://gitlab.com/ci
Please enter the gitlab-ci token for this runner:
********************
Please enter the gitlab-ci description for this runner:
[cori07]: cori
Please enter the gitlab-ci tags for this runner (comma separated):
cori,nersc
Whether to run untagged builds [true/false]:
[false]: 
Whether to lock Runner to current project [true/false]:
[false]: 
Registering runner... succeeded                     runner=akxuUWGy
Please enter the executor: parallels, virtualbox, docker+machine, kubernetes, docker, docker-ssh, shell, ssh, docker-ssh+machine:
shell
Runner registered successfully. Feel free to start it, but if it's running already the config should be automatically reloaded!
```

Start the runner, leave the terminal window open:

```
export WD=$HOME/gitlab-runner/$NERSC_HOST
~/bin/gitlab-runner run \
  --config $HOME/.gitlab-runner/config.$NERSC_HOST.toml \
  --working-directory $WD
```

Reload the **Settings** -> **CI/CD Pipelines** tab, you should see your runner listed there, with the tags you gave it.

Now, copy **05-gitlab-ci.yml** to **.gitlab-ci.yml**, edit to change the **REGISTRY_USER** to yourself and the **tags** to **cori,nersc**, commit the file and see what happens.

You should see the pipeline logfile shows the runner running as you, on cori. If you log into cori on a second terminal, you should find a new file, **Gitlab-was-here**, in your home directory.

Stop your runner by hitting CTRL-C, twice. Don't leave it running unless you know exactly what you're doing!

Your runner has access to all the filesystems you have access to (it's you, after all). By default it will start in $HOME/gitlab-runner/$NERSC_HOST if you follow the above commands. It will keep that directory clean by itself, but if you 'cd' somewhere else (e.g. $SCRATCH), it's up to you to make sure your build starts in a clean directory.

## Best practices ##
- Don't do this unless you _really_ have to
- Don't do this with a shared NERSC account, get a dedicated account for your team/project if you can
- Don't do this without checking with a consultant or a security specialist first
- Don't _ever_ do this with a repository that anyone else can commit to directly (that's equivalent to sharing your password with them)
- Don't share your registration token with other users

If you do suspect your runner may be compromised, you can take steps to mitigate the effect:
- clone your project, inspect the code to make sure it hasn't been modified
- delete your project from gitlab, re-create it
- verify that this give you a different registration token from before
- delete your old runner, start a new one